// Prénoms, noms et matricule des membres de l'équipe:
// - Christian Harper-Cyr (1680017)
// - Jean-Alexandre Barszcz (1692955)

#include <iostream>
#include <stdlib.h>
#include <GL/glew.h>
#include <GL/glut.h>
#include <ctime>
#include "varglob.h"

struct Particule
{
   GLfloat position[3];          // en unités
   GLfloat vitesse[3];           // en unités/seconde
   GLfloat tempsDeVieRestant;    // en secondes
   GLubyte couleurActuelle[4];   // couleur actuelle de la particule
   GLubyte couleur[4];
   bool    estSelectionnee;      // la particule est actuellement sélectionnée
   // (vous pouvez ajouter d'autres éléments)
};
const unsigned int MAXNPARTICULES = 50000;
Particule particules[MAXNPARTICULES]; // tableau de particules

unsigned int nparticules = 300;  // nombre de particules utilisées (actuellement affichées)
float tempsVieMax = 5;           // temps de vie maximal (en secondes)

float gravite = 0.5; // gravité utilisée dans le calcul de la position de la particule

const GLubyte COULMIN =  50; // valeur minimale d'une composante de couleur lorsque la particule (re)naît
const GLubyte COULMAX = 250; // valeur maximale d'une composante de couleur lorsque la particule (re)naît

const float positionPuits[3] = { 0, 0.5, 0 }; // position du puits de particules

bool modeSelection = false; // on est en mode sélection
int dernierX=0, dernierY=0;

bool transparenceSelonAge = false; // l'âge de la particule est utilisée pour la rendre transparente
bool couleurSelonNCollisions = false; // la couleur de la particules dépend du nombre de collisions

bool boutonClicke = false;

// Valeur aléatoire entre 0.0 et 1.0
float myrandom()
{
   return (float) rand() / (float) RAND_MAX;
}

void regenererParticule(Particule& part)
{
	// Position au puit
	part.position[0] = positionPuits[0];
	part.position[1] = positionPuits[1];
	part.position[2] = positionPuits[2];
	// Direction aleatoire (vecteur unitaire)
	do
	{
		part.vitesse[0] = myrandom() - 0.5;
		part.vitesse[1] = myrandom() - 0.5;
		part.vitesse[2] = myrandom() - 0.5;
	} while (part.vitesse[0] == 0 && part.vitesse[1] == 0 && part.vitesse[2] == 0);
	GLfloat length = sqrtf(part.vitesse[0] * part.vitesse[0] + part.vitesse[1] * part.vitesse[1] + part.vitesse[2] * part.vitesse[2]);
	part.vitesse[0] /= length;
	part.vitesse[1] /= length;
	part.vitesse[2] /= length;
	part.tempsDeVieRestant = myrandom()*5.0;
}

void updateParticule(Particule& part, float deltaT)
{
	// On enleve le deltaT du temps
	part.tempsDeVieRestant -= deltaT;
	if (part.tempsDeVieRestant <= 0.0)
	{
		regenererParticule(part);
		return;
	}
	// On actualise la position (p = p0 + vt)
	part.position[0] += part.vitesse[0] * deltaT;
	part.position[1] += part.vitesse[1] * deltaT;
	part.position[2] += part.vitesse[2] * deltaT;
	if (part.position[0] <= -1 || part.position[0] >= 1)
		part.vitesse[0] = -part.vitesse[0];
	if (part.position[1] <= 0 || part.position[1] >= 2)
		part.vitesse[1] = -part.vitesse[1];
	if (part.position[2] <= -1 || part.position[2] >= 1)
		part.vitesse[2] = -part.vitesse[2];
	// On actualise la vitesse (v = v0 + gt) (seulement en y)
	part.vitesse[1] -= gravite*deltaT;
}

void avancerParticules( const int deltaT )
{
   for ( unsigned int i = 0 ; i < nparticules ; i++ )
   {
	   updateParticule(particules[i], (GLfloat)deltaT/1000.f);
   }
}

int deltaTemps=0;
static void animer( int tempsPrec )
{
   // obtenir le temps depuis le début du programme, en millisecondes
   int tempsCour = glutGet( GLUT_ELAPSED_TIME );
   if ( tempsPrec == 0 ) tempsPrec = tempsCour;

   // temps d'attente en secondes avant le prochain affichage
   const int FPS = 60;  // en "images/seconde"
   const int delai = 1000/FPS;  // en "millisecondes/image" (= 1000 millisecondes/seconde  /  images/seconde)
   if ( enmouvement ) glutTimerFunc( delai, animer, tempsCour );

   deltaTemps = tempsCour - tempsPrec;

   // déplacer les particules
   avancerParticules( deltaTemps );

   // indiquer qu'il faut afficher à nouveau
   glutPostRedisplay();
}

void initialisation()
{
   enmouvement = true;

   // donner la position de la caméra
   theta = 0.0;
   phi = 1.0;
   dist = 5.0;

   // donner la couleur de fond
   glClearColor( 0.0, 0.0, 0.0, 1.0 );

   // activer les etats openGL
   glEnable( GL_DEPTH_TEST );
   glEnable(GL_POINT_SMOOTH);

   // Initialisation des particules
   for ( unsigned int i = 0 ; i < MAXNPARTICULES ; i++ )
   {
	   regenererParticule(particules[i]);
	   particules[i].couleur[0] = myrandom()*(COULMAX - COULMIN) + COULMIN;
	   particules[i].couleur[1] = myrandom()*(COULMAX - COULMIN) + COULMIN;
	   particules[i].couleur[2] = myrandom()*(COULMAX - COULMIN) + COULMIN;
	   particules[i].couleurActuelle[0] = particules[i].couleur[0];
	   particules[i].couleurActuelle[1] = particules[i].couleur[1];
	   particules[i].couleurActuelle[2] = particules[i].couleur[2];
	   particules[i].estSelectionnee = false;
   }

   glewInit();

   srand(time(NULL));
}

void afficherScene()
{
   glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

   glMatrixMode( GL_PROJECTION );
   glLoadIdentity();
   gluPerspective( 45.0, (GLdouble) g_largeur / (GLdouble) g_hauteur, 0.1, 10.0 );

   glMatrixMode( GL_MODELVIEW );
   glLoadIdentity();
   gluLookAt( dist*sin(phi)*sin(theta), dist*cos(phi), dist*sin(phi)*cos(theta), 0, 1, 0, 0, 2, 0 );

   glEnableClientState( GL_VERTEX_ARRAY );
   glEnableClientState( GL_COLOR_ARRAY );

   // afficher la boîte
   const GLfloat coo[] = { -1, 0,  1,   1, 0,  1,   1, 0, -1,  -1, 0, -1,
                            1, 2,  1,   1, 2, -1,   1, 0, -1,   1, 0,  1,
                           -1, 2,  1,  -1, 2, -1,  -1, 0, -1,  -1, 0,  1,
                            1, 2, -1,  -1, 2, -1,  -1, 0, -1,   1, 0, -1 };
   const GLubyte couleur[] = { 255, 255, 255,  255, 255, 255,  255, 255, 255,  255, 255, 255,
                               255, 255,   0,  255, 255,   0,  255, 255,   0,  255, 255,   0,
                               255,   0, 255,  255,   0, 255,  255,   0, 255,  255,   0, 255,
                               0, 255, 255,    0, 255, 255,    0, 255, 255,    0, 255, 255 };

   glVertexPointer( 3, GL_FLOAT, 0, coo );
   glColorPointer( 3, GL_UNSIGNED_BYTE, 0, couleur );
   glDrawArrays( GL_QUADS, 0, 16 );

   glPointSize(10);

   // On trace les particules
   glVertexPointer(3, GL_FLOAT, sizeof(Particule), particules);
   glColorPointer(4, GL_UNSIGNED_BYTE, sizeof(Particule), ((char*)particules) + 28); // La couleur est au byte 28 de la structure
   glDrawArrays(GL_POINTS, 0, nparticules);

   glDisableClientState(GL_VERTEX_ARRAY);
   glDisableClientState(GL_COLOR_ARRAY);

   glutSwapBuffers();
}

void redimensionnement( GLsizei w, GLsizei h )
{
   g_largeur = w;
   g_hauteur = h;
   glViewport( 0, 0, w, h );
   glutPostRedisplay();
}

void clavier( unsigned char touche, int x, int y )
{
   switch ( touche )
   {
   case 27:
   case '\e': // escape
      glutDestroyWindow( g_feneID );
      exit( 0 );
      break;
   case ' ':
      enmouvement = !enmouvement;
      if ( enmouvement ) animer( 0 );
      glutPostRedisplay();
      break;
   case '[':
      nparticules /= 1.2;
      if ( nparticules < 5 ) nparticules = 5;
      std::cout << " nparticules=" << nparticules << std::endl;
      glutPostRedisplay();
      break;
   case ']':
      nparticules *= 1.2;
      if ( nparticules > MAXNPARTICULES ) nparticules = MAXNPARTICULES;
      std::cout << " nparticules=" << nparticules << std::endl;
      glutPostRedisplay();
      break;
   case 'a':
      gravite += 0.05;
      std::cout << " gravite=" << gravite << std::endl;
      glutPostRedisplay();
      break;
   case 'd':
      gravite -= 0.05;
      if ( gravite < 0.0 ) gravite = 0.0;
      std::cout << " gravite=" << gravite << std::endl;
      glutPostRedisplay();
      break;
   case 'w':
      tempsVieMax += 1.0;
      std::cout << " tempsVieMax=" << tempsVieMax << std::endl;
      glutPostRedisplay();
      break;
   case 's':
      tempsVieMax -= 1.0;
      if ( tempsVieMax < 1.0 ) tempsVieMax = 1.0;
      std::cout << " tempsVieMax=" << tempsVieMax << std::endl;
      glutPostRedisplay();
      break;
   case 'p':
      for ( unsigned int i = 0 ; i < nparticules ; i++ )
      {
         if ( particules[i].estSelectionnee )
         {
			 regenererParticule(particules[i]);
         }
      }
      glutPostRedisplay();
      break;
   case 't':
      transparenceSelonAge = !transparenceSelonAge;
      glutPostRedisplay();
      break;
   case 'c':
      couleurSelonNCollisions = !couleurSelonNCollisions;
      glutPostRedisplay();
      break;
   }
}

void clavierSpecial( int touche, int x, int y )
{
}

void sourisClic( int button, int state, int x, int y )
{
	if (button == GLUT_LEFT_BUTTON)
	{
		if (state == GLUT_DOWN)
		{
			dernierX = x;
			dernierY = y;
		}
	}
	else if (button == GLUT_RIGHT_BUTTON)
	{
		static int downPosX = 0;
		static int downPosY = 0;
		if (state == GLUT_DOWN)
		{
			boutonClicke = true;
			downPosX = x;
			downPosY = y;
		}
		else if (state == GLUT_UP)
		{
			boutonClicke = false;
			if (abs(downPosX - x) <= 3 && abs(downPosY - y) <= 3)
			{
				GLubyte pixel[4];
				glReadBuffer(GL_FRONT);
				glReadPixels(downPosX, g_hauteur-downPosY, 1, 1, GL_RGBA, GL_UNSIGNED_BYTE, pixel);
				for (int i = 0; i < nparticules; i++)
				{
					Particule& p = particules[i];
					if (p.couleur[0] == pixel[0] && p.couleur[1] == pixel[1] && p.couleur[2] == pixel[2])
					{
						p.estSelectionnee = true;
						p.couleurActuelle[0] = 0;
						p.couleurActuelle[1] = 0;
						p.couleurActuelle[2] = 0;
						p.couleurActuelle[3] = 0;
						printf("Selectionne particule '%d'\n", i);
						break;
					}
				}
			}
		}
	}
}

void sourisMouvement( int x, int y )
{
	if (boutonClicke)
		return;
   theta += (dernierX-x) / 100.0;
   phi += (dernierY-y) / 50.0;
   dernierX = x;
   dernierY = y;

   if ( phi >= M_PI )
      phi = M_PI - 0.001;
   else if ( phi <= 0 )
      phi = 0.001;

   glutPostRedisplay();
}

int main( int argc, char *argv[] )
{
   // initialisation de GLUT
   glutInit( &argc, argv );
   // paramétrage de l'affichage
   glutInitDisplayMode( GLUT_RGBA | GLUT_DEPTH | GLUT_DOUBLE );
   // taille de la fenetre
   glutInitWindowSize( g_largeur, g_hauteur );
   // création de la fenêtre
   g_feneID = glutCreateWindow( "INF2705" );

   // référencement de la fonction de rappel pour l'affichage
   glutDisplayFunc( afficherScene );
   // référencement de la fonction de rappel pour le redimensionnement
   glutReshapeFunc( redimensionnement );
   // référencement de la fonction de gestion des touches
   glutKeyboardFunc( clavier );
   // référencement de la fonction de gestion des touches spéciales
   glutSpecialFunc( clavierSpecial );
   // référencement de la fonction de rappel pour le mouvement de la souris
   glutMotionFunc( sourisMouvement );
   // référencement de la fonction de rappel pour le clic de la souris
   glutMouseFunc( sourisClic );

   initialisation();
   if ( enmouvement ) animer( 0 );

   // boucle principale de gestion des evenements
   glutMainLoop();

   // le programme n'arrivera jamais jusqu'ici
   return EXIT_SUCCESS;
}
