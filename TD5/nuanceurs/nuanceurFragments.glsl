// les étudiants peuvent utiliser l'exemple du cours pour démarrer:
//    http://www.cours.polymtl.ca/inf2705/nuanceurs/exempleIllumination/phong.glsl

#version 130

in vec4 pos;
in vec3 normal;
in vec2 textCoord;

// Les paramètres de la source de lumière sont définis ainsi:
// struct gl_LightSourceParameters
// {
//    vec4 ambient;
//    vec4 diffuse;
//    vec4 specular;
//    vec4 position;
//    vec4 halfVector;
//    vec3 spotDirection;
//    float spotExponent;
//    float spotCutoff;          // ( [0.0,90.0], 180.0 )
//    float spotCosCutoff;       // == cos(spotCutoff) ( [1.0,0.0], -1.0 )
//    float constantAttenuation;
//    float linearAttenuation;
//    float quadraticAttenuation;
// };
// uniform gl_LightSourceParameters gl_LightSource[gl_MaxLights];
// bool gl_FrontFacing  // on est en train de tracer la face avant?

uniform bool utiliseBlinn;
uniform bool utiliseDirect;
uniform bool localViewer;

uniform int laTexture;
uniform bool utiliseCouleur;
uniform int texnumero;
uniform bool noirTransparent;

uniform vec3 eyePos;

uniform sampler2D text;

void main( void )
{
	gl_FragColor = vec4(0,0,0,1);
	gl_MaterialParameters material;
	
	if(gl_FrontFacing)
	{
		material = gl_FrontMaterial;
	}
	else
	{
		material = gl_BackMaterial;
	}
	
	if(texnumero != 0)
	{
		vec3 couleur = vec3(texture(text, textCoord));
		if(utiliseCouleur)
		{
			if(couleur == vec3(0,0,0))
			{
				if(noirTransparent)
					discard;
				else
					material.diffuse.rgb = vec3(0,0,0);
			}
			else
				material.diffuse.rgb = couleur*material.diffuse.rgb;
		}
		else
		{
			if(couleur == vec3(0,0,0) && noirTransparent)
				discard;
			else
				material.diffuse.rgb = couleur;
		}
	}
	for(int i = 0; i < gl_MaxLights; i++)
	{
		// On calcule la lumiere
		vec4 dir = pos-gl_LightSource[i].position;
		vec4 ndir = normalize(dir);
		dir.w = 0;
		float dist = length(dir);
		float spotExp = 1;
		vec4 color = vec4(0,0,0,1);
		// On calcule le spot
		if(utiliseDirect)
		{
			float cosG = dot(vec3(ndir),normalize(gl_LightSource[i].spotDirection));
			if(cosG > gl_LightSource[i].spotCosCutoff)
			{
				float cosInner = gl_LightSource[i].spotCosCutoff;
				float cosOuter = pow(cosInner, 1.01 + (gl_LightSource[i].spotExponent/2.0));
				spotExp = (cosG-cosOuter)/(cosInner-cosOuter);
			}
			else
				spotExp = 0;
		}
		else
		{
			float cosG = dot(vec3(ndir),normalize(gl_LightSource[i].spotDirection));
			if(cosG > gl_LightSource[i].spotCosCutoff)
				spotExp = pow(cosG,gl_LightSource[i].spotExponent);
				//spotExp = cosG;
			else
				spotExp = 0;
		}
		
		float attenuation = min(1, 1.0/(gl_LightSource[i].constantAttenuation + gl_LightSource[i].linearAttenuation*dist + gl_LightSource[i].quadraticAttenuation*dist*dist));
		//Ambient
		color += max(gl_LightSource[i].ambient*material.ambient, vec4(0,0,0,0));
		// Diffuse
		color += max(attenuation*gl_LightSource[i].diffuse*material.diffuse*dot(ndir,vec4(normal,0)), vec4(0,0,0,0));
		// Specular
		//*
		vec4 V = vec4(eyePos,1)-pos;
		if(utiliseBlinn)
		{
			vec3 B = normalize(vec3(V-dir));
			color += max(attenuation*gl_LightSource[i].specular*material.specular*pow(dot(B,normal),material.shininess), vec4(0,0,0,0));
		}
		else
		{
			vec4 R = reflect(ndir, normalize(vec4(normal, 0)));
			V = normalize(V);
			color += max(attenuation*gl_LightSource[i].specular*material.specular*pow(dot(R,V),material.shininess), vec4(0,0,0,0));
		}
		//*/
		gl_FragColor += color*spotExp;
	}
	gl_FragColor = clamp(gl_FragColor,0.0,1.0);
}
