// les étudiants peuvent utiliser l'exemple du cours pour démarrer:
//    http://www.cours.polymtl.ca/inf2705/nuanceurs/exempleIllumination/phong.glsl

#version 130

out vec4 pos;
out vec3 normal;
out vec2 textCoord;

void main( void )
{
   // transformation standard du sommet (ModelView et Projection)
   gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
   normal = -normalize(gl_NormalMatrix*gl_Normal); // Pour une raison, les normales sont inversees
   pos = gl_ModelViewMatrix*gl_Vertex;
   textCoord = vec2(gl_MultiTexCoord0);
}
