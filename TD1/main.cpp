// Prénoms, noms et matricule des membres de l'équipe:
// - Prénom1 NOM1 (matricule1)
// - Prénom2 NOM2 (matricule2)

#include <iostream>
#include <GL/glut.h>
#include "varglob.h"
#include "float3.h"

#if 1
// pour un W
float p1[3] = { -4.0,  2.0,  0.0 };
float p2[3] = { -3.0, -3.0,  0.0 };
float p3[3] = { -1.0, -3.0,  0.0 };
float p4[3] = {  0.0,  0.0,  0.0 };
float p5[3] = {  1.0, -3.0,  0.0 };
float p6[3] = {  3.0, -3.0,  0.0 };
float p7[3] = {  4.0,  2.0,  0.0 };
#else
// pour une flèche (Voir apprentissage supplémentaire)
float p1[3] = { -3.0,  1.0,  0.0 };
float p2[3] = { -3.0, -1.0,  0.0 };
float p3[3] = {  0.0, -1.0,  0.0 };
float p4[3] = { -0.5, -2.5,  0.0 };
float p5[3] = {  3.0,  0.0,  0.0 };
float p6[3] = { -0.5,  2.5,  0.0 };
float p7[3] = {  0.0,  1.0,  0.0 };
#endif

uint8_t colors[6][3] = {{255,0,0},{0,255,0},{0,0,255},{255,255,0},{255,0,255},{0,255,255}};
uint8_t colorIndex = 0;

bool wireframed = false;

void drawPants()
{
   glPushMatrix();
   glScalef(0.9,0.9,0.9);
   glBegin(GL_TRIANGLE_FAN);
   glVertex2f(0,0);
   glVertex2f(0.25, -1);
   glVertex2f(0.75, -1);
   glVertex2f(1,1);
   glVertex2f(-1,1);
   glVertex2f(-0.75, -1);
   glVertex2f(-0.25, -1);
   glEnd();
   glPopMatrix();
   // glPushMatrix();
   // glScalef(0.9,0.9,0.9);
   // glBegin(GL_QUADS);
   // glVertex2f(-1,1);
   // glVertex2f(-0.75, -1);
   // glVertex2f(-0.25, -1);
   // glVertex2f(0,0);
   // glVertex2f(0,0);
   // glVertex2f(0.25, -1);
   // glVertex2f(0.75, -1);
   // glVertex2f(1,1);
   // glEnd();
   // glBegin(GL_TRIANGLES);
   // glVertex2f(-1,1);
   // glVertex2f(0,0);
   // glVertex2f(1,1);
   // glEnd();
   // glPopMatrix();
}
void drawLines()
{
   glColor3ub(128, 128, 128);
   glBegin(GL_LINES);
   glVertex2f(-1,0);
   glVertex2f(1,0);
   glVertex2f(-1.0/3.0,-1);
   glVertex2f(-1.0/3.0,1);
   glVertex2f(1.0/3.0,-1);
   glVertex2f(1.0/3.0,1);
   glEnd();
}


void initialisation()
{
   // donner la couleur de fond
   glClearColor( 0.0, 0.0, 0.0, 1.0 );

   // définir le pipeline graphique
   glMatrixMode( GL_PROJECTION );
   glLoadIdentity();
   //glOrtho( -12, 12, -8, 8, -10, 10 );
   glMatrixMode( GL_MODELVIEW );
   glLoadIdentity();

   // activer le mélange de couleur pour bien voir les possibles plis à l'affichage
   glEnable( GL_BLEND );
   glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
}

void afficherScene()
{
   glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

   glLoadIdentity();
   drawLines();

   glScalef(1.0/3.0,1.0/2.0,1);

   glTranslatef(-2,1.0,0);
   glColor3ub(colors[colorIndex][0],colors[colorIndex][1],colors[colorIndex][2]);
   drawPants();

   glTranslatef(2,0,0);
   glColor3ub(colors[(colorIndex+1)%6][0],colors[(colorIndex+1)%6][1],colors[(colorIndex+1)%6][2]);
   drawPants();

   glTranslatef(2,0,0);
   glColor3ub(colors[(colorIndex+2)%6][0],colors[(colorIndex+2)%6][1],colors[(colorIndex+2)%6][2]);
   drawPants();

   glTranslatef(-4,-2,0);
   glColor3ub(colors[(colorIndex+3)%6][0],colors[(colorIndex+3)%6][1],colors[(colorIndex+3)%6][2]);
   drawPants();

   glTranslatef(2,0,0);
   glColor3ub(colors[(colorIndex+4)%6][0],colors[(colorIndex+4)%6][1],colors[(colorIndex+4)%6][2]);
   drawPants();
   
   glTranslatef(2,0,0);
   glColor3ub(colors[(colorIndex+5)%6][0],colors[(colorIndex+5)%6][1],colors[(colorIndex+5)%6][2]);
   drawPants();

   glutSwapBuffers();
}

void redimensionnement( GLsizei w, GLsizei h )
{
   g_largeur = w;
   g_hauteur = h;
   glViewport( 0, 0, w, h );
   glutPostRedisplay();
}

void clavier( unsigned char touche, int x, int y )
{
   switch ( touche )
   {
   case '\e': // escape
      glutDestroyWindow( g_feneID );
      exit( 0 );
      break;
   case 'f':
      glutPostRedisplay(); // indiquer que la fenêtre a besoin d'être réafficher
      break;
   case 'n':
      colorIndex = (colorIndex + 1)%6;
      glutPostRedisplay();
      break;
   case 'w':
      wireframed = !wireframed;
      if(wireframed)
         glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );
      else
         glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );
      glutPostRedisplay();
   }
}

void clavierSpecial( int touche, int x, int y )
{
}

void sourisClic( int button, int state, int x, int y )
{
}

void sourisMouvement( int x, int y )
{
}

int main( int argc, char *argv[] )
{
   // initialisation de GLUT
   glutInit( &argc, argv );
   // paramétrage de l'affichage
   glutInitDisplayMode( GLUT_RGBA | GLUT_DEPTH | GLUT_DOUBLE );
   // taille de la fenetre
   glutInitWindowSize( g_largeur, g_hauteur );
   // création de la fenêtre
   g_feneID = glutCreateWindow( "INF2705" );

   // référencement de la fonction de rappel pour l'affichage
   glutDisplayFunc( afficherScene );
   // référencement de la fonction de rappel pour le redimensionnement
   glutReshapeFunc( redimensionnement );
   // référencement de la fonction de gestion des touches
   glutKeyboardFunc( clavier );
   // référencement de la fonction de gestion des touches spéciales
   glutSpecialFunc( clavierSpecial );
   // référencement de la fonction de rappel pour le mouvement de la souris
   glutMotionFunc( sourisMouvement );
   // référencement de la fonction de rappel pour le clic de la souris
   glutMouseFunc( sourisClic );

   initialisation();

   // boucle principale de gestion des événements
   glutMainLoop();

   // le programme n'arrivera jamais jusqu'ici
   return EXIT_SUCCESS;
}
