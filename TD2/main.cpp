// Prénoms, noms et matricule des membres de l'équipe:
// - Christian Harper-Cyr (1680017)
// - Jean-Alexandre Barszcz (1692955)

#include <iostream>
#include <GL/glew.h>
#include <GL/glut.h>
#include "varglob.h"
#include "float3.h"
#include "teapot_data.h"

// partie 1:
double thetaBras = 0.0;   // angle de rotation du bras
double phiBras = 0.0;     // angle de rotation du bras
double angleTheiere = 0.0;

// partie 2:
GLdouble MAXPHI = M_PI - 0.001, MAXTHETA = M_PI - 0.001;
GLdouble MINPHI = 0.001, MINTHETA = 0.001;

GLuint g_VBOsommets = 0;
GLuint g_VBOconnec = 0;

// Position de la camera en coordonnees spheriques
double phiCam = 0.0;
double distCam = 15.811388301;
double thetaCam = 51.057558732;

const double minThetaCam = 0.01;
const double maxThetaCam = M_PI - M_PI/16;

const double minDistCam = 6;

bool modeLookAt = true;


void creerVBO()
{
   // On genere les buffers
   glGenBuffers(1, &g_VBOsommets);
   glGenBuffers(1, &g_VBOconnec);
   
   // On rempli les buffers
   glBindBuffer(GL_ARRAY_BUFFER, g_VBOsommets);
   glBufferData(GL_ARRAY_BUFFER, sizeof(gTeapotSommets), gTeapotSommets, GL_STATIC_DRAW);

   glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, g_VBOconnec);
   glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(gTeapotConnec), gTeapotConnec, GL_STATIC_DRAW);
}

void initialisation()
{
   theta = 0.;
   phi = 1.;
   dist = 10.;

   // donner la couleur de fond
   glClearColor( 0.0, 0.0, 0.0, 1.0 );

   // activer les etats openGL
   glEnable( GL_DEPTH_TEST );

   glewInit();

   creerVBO();
}

// (partie 1) Vous devez vous servir de ces deux fonctions (sans les modifier) pour tracer les quadriques.
void afficherCylindre( )
{
   // affiche un cylindre de rayon 1 et de hauteur 1
   static GLUquadric* q = 0;
   if ( !q ) q = gluNewQuadric();
   const GLint slices = 16, stack = 2;
   glColor3f( 0, 0, 1 );
   gluCylinder( q, 1.0, 1.0, 1.0, slices, stack );
}
void afficherSphere( )
{
   // affiche une sphere de rayon 1
   static GLUquadric* q = 0;
   if ( !q ) q = gluNewQuadric();
   const GLint slices = 16, stack = 32;
   glColor3f( 1, 0, 0 );
   gluSphere( q, 1.0, slices, stack );
}

// (partie 2) Vous modifierez cette fonction pour utiliser les VBOs
void afficherTheiere()
{
   // Vert
   glColor3f( 0.0, 1.0, 0.0 );
   
   glEnableClientState(GL_VERTEX_ARRAY);
   
   // On bind les buffers de donnees
   glBindBuffer(GL_ARRAY_BUFFER, g_VBOsommets);
   glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, g_VBOconnec);
   
   // On utilise les vertexs de la theiere
   glVertexPointer(3, GL_FLOAT, 0, 0);
   
   // On dessine
   glDrawElements(GL_TRIANGLES, 1024*3, GL_UNSIGNED_INT, 0);
   
   // On relache les buffers de donnees
   glBindBuffer(GL_ARRAY_BUFFER, 0);
   glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

   glDisableClientState(GL_VERTEX_ARRAY);
}

void afficherBras()
{
   // On dessine chaque partie du bras
   const GLfloat cylhauteur = 2.0;
   const GLfloat sphererayon = 0.25;

   glPushMatrix();
   glScalef(sphererayon, sphererayon, cylhauteur);
   afficherCylindre();
   glPopMatrix();

   glTranslatef(0.f, 0.f, cylhauteur);
   glRotatef(phiBras, 0, 0, 1);

   glPushMatrix();
   glScalef(sphererayon, sphererayon, sphererayon);
   afficherSphere();
   glPopMatrix();

   glRotatef(90, 0, 1, 0);

   glPushMatrix();
   glScalef(sphererayon, sphererayon, cylhauteur);
   afficherCylindre();
   glPopMatrix();

   glTranslatef(0,0,cylhauteur);

   glPushMatrix();
   glScalef(sphererayon, sphererayon, sphererayon);
   afficherSphere();
   glPopMatrix();

   glRotatef(-90, 1, 0, 0);
   glRotatef(thetaBras, 1, 0, 0);


   glPushMatrix();
   glScalef(sphererayon, sphererayon, cylhauteur);
   afficherCylindre();
   glPopMatrix();

   glTranslatef(0,0,cylhauteur);

   glPushMatrix();
   glScalef(sphererayon, sphererayon, sphererayon);
   afficherSphere();
   glPopMatrix();

   glRotatef(90, 0, 0, 1);
   glRotatef(-90, 0, 1, 0);
   glRotatef(angleTheiere, 0, 0, 1);

   glTranslatef(1.5f,0,0);

   glPushMatrix();
   glTranslatef(-0.1, -0.65, 0);
   glScalef(0.3, 0.3, 0.3);
   afficherTheiere();
   glPopMatrix();

   glPopMatrix();

}

void definirCamera()
{
	if(modeLookAt)
   		gluLookAt( distCam*sin(phiCam)*sin(thetaCam), distCam*cos(phiCam)*sin(thetaCam), distCam*cos(thetaCam),  0, 0, 0,  0, 0, 1 );
   	else
   	{
		glRotated(180, 0, 0, 1);
		glRotated(-phiCam/M_PI*180, 0, 1, 0);
		glRotated(thetaCam/M_PI*180, 1, 0, 0);
   		glTranslated(-distCam*sin(phiCam)*sin(thetaCam), -distCam*cos(phiCam)*sin(thetaCam), -distCam*cos(thetaCam));
   	}

}	

void afficherScene()
{
   glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

   glMatrixMode( GL_PROJECTION );
   glLoadIdentity();
   gluPerspective( 45.0, (GLdouble) g_largeur / (GLdouble) g_hauteur, 0.1, 300.0 );

   glMatrixMode( GL_MODELVIEW );
   glLoadIdentity();

   definirCamera();

   glColor3f( 1., .5, .5 );
   glBegin( GL_QUADS );
   glVertex3f( -4.0,  4.0, 0.0 );
   glVertex3f(  4.0,  4.0, 0.0 );
   glVertex3f(  4.0, -4.0, 0.0 );
   glVertex3f( -4.0, -4.0, 0.0 );
   glEnd();

   afficherBras();


   glutSwapBuffers();
}

void redimensionnement( GLsizei w, GLsizei h )
{
   g_largeur = w;
   g_hauteur = h;
   glViewport( 0, 0, w, h );
   glutPostRedisplay();
}

void clavier( unsigned char touche, int x, int y )
{
   switch ( touche )
   {
   case 27: // escape
      glutDestroyWindow( g_feneID );
      exit( 0 );
      break;
   case '-':
   case '_':
      distCam += 0.1;
      break;
   case '+':
   case '=':
   		distCam -= 0.1;
   		if(distCam < minDistCam)
   			distCam = minDistCam;
      break;
   case 'r':
   		phiCam = 0.0;
		distCam = 15.811388301;
		thetaCam = 90-2*19.471220634;
      break;
   case 'l':
      modeLookAt = !modeLookAt;
      std::cout << " modeLookAt=" << modeLookAt << std::endl;
      break;
   case 'g':
      {
         static bool modePlein = true;
         modePlein = !modePlein;

		   if(modePlein)
		   {
		   		glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );
		   }
		   else
		   {
		   		glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );
		   }
      }
      break;
   case '[':
      angleTheiere -= 1.0;
      break;
   case ']':
      angleTheiere += 1.0;
      break;
   }
   glutPostRedisplay();
}

void clavierSpecial( int touche, int x, int y )
{
   switch ( touche )
   {
   case GLUT_KEY_LEFT:
      phiBras -= 2.0;
      if(phiBras < MINPHI*180/M_PI)
      	phiBras = MINPHI*180/M_PI;
      break;
   case GLUT_KEY_RIGHT:
      phiBras += 2.0;
      if(phiBras > MAXPHI*180/M_PI)
      	phiBras = MAXPHI*180/M_PI;
      break;
   case GLUT_KEY_DOWN:
      thetaBras -= 2.0;
      if(thetaBras < MINTHETA*180/M_PI)
      	thetaBras = MINTHETA*180/M_PI;
      break;
   case GLUT_KEY_UP:
      thetaBras += 2.0;
      if(thetaBras > MAXTHETA*180/M_PI)
      	thetaBras = MAXTHETA*180/M_PI;
      break;
   }
   glutPostRedisplay();
}

int dernierX, dernierY;
void sourisClic( int button, int state, int x, int y )
{
   // button est un parmi { GLUT_LEFT_BUTTON, GLUT_MIDDLE_BUTTON, GLUT_RIGHT_BUTTON }
   // state  est un parmi { GLUT_DOWN, GLUT_UP }
   // Pour référence et quelques exemples, voir http://www.lighthouse3d.com/opengl/glut/index.php?9
   if ( state == GLUT_DOWN )
   {
      dernierX = x;
      dernierY = y;
   }
}

void sourisMouvement( int x, int y )
{
	if(x-dernierX < 0)
		phiCam -= 0.02;
	else
		phiCam += 0.02;
	if(y-dernierY<0)
	{
		thetaCam += 0.02;
		if(thetaCam > maxThetaCam)
			thetaCam = maxThetaCam;
	}
	else
	{
		thetaCam -= 0.02;
		if(thetaCam < minThetaCam)
			thetaCam = minThetaCam;
	}
   dernierX = x;
   dernierY = y;

   glutPostRedisplay();
}

int main( int argc, char *argv[] )
{
   // initialisation de GLUT
   glutInit( &argc, argv );
   // paramétrage de l'affichage
   glutInitDisplayMode( GLUT_RGBA | GLUT_DEPTH | GLUT_DOUBLE );
   // taille de la fenetre
   glutInitWindowSize( g_largeur, g_hauteur );
   // création de la fenêtre
   g_feneID = glutCreateWindow( "INF2705" );

   // référencement de la fonction de rappel pour l'affichage
   glutDisplayFunc( afficherScene );
   // référencement de la fonction de rappel pour le redimensionnement
   glutReshapeFunc( redimensionnement );
   // référencement de la fonction de gestion des touches
   glutKeyboardFunc( clavier );
   // référencement de la fonction de gestion des touches spéciales
   glutSpecialFunc( clavierSpecial );
   // référencement de la fonction de rappel pour le mouvement de la souris
   glutMotionFunc( sourisMouvement );
   // référencement de la fonction de rappel pour le clic de la souris
   glutMouseFunc( sourisClic );

   initialisation();

   // boucle principale de gestion des evenements
   glutMainLoop();

   // le programme n'arrivera jamais jusqu'ici
   return EXIT_SUCCESS;
}
