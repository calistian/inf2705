// les étudiants peuvent utiliser l'exemple du cours pour démarrer:
//    http://www.cours.polymtl.ca/inf2705/nuanceurs/exempleSimple/

#version 130

#define M_PI_2  (1.57079632679489661923)  // PI/2

out vec4 position;

void main( void )
{
   // transformation standard du sommet (ModelView et Projection)
	position = gl_Vertex;
   gl_Position = gl_ProjectionMatrix * gl_ModelViewMatrix * gl_Vertex;
   //gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;

}
