// les étudiants peuvent utiliser l'exemple du cours pour démarrer:
//    http://www.cours.polymtl.ca/inf2705/nuanceurs/exempleSimple/

#version 130

// couleur du pôle sans réchauffement
vec4 couleurPole = vec4( 1.0, 1.0, 1.0, 1.0 );

in vec4 position;

uniform float facteurRechauffement;
uniform vec3 color; // La couleur de base de la planete
uniform float rayon; // Le rayon de la planete

void main( void )
{
	float a = abs(position.z) / rayon;
	vec4 couleurEq = vec4(color, 1);
	vec4 couleurPole = mix(couleurEq, vec4(1, 1, 1, 1), facteurRechauffement);
	gl_FragColor = mix(couleurEq, couleurPole, vec4(a, a, a, a));
}
