#version 130

uniform sampler2D colorMap;
uniform int indiceCouleur;
uniform vec3 eyePos;

in vec4 pos;
in vec3 normal;
in vec2 texCoord;

uniform sampler2D sampler;

void main( void )
{
	gl_FragColor = vec4(0,0,0,1);
	gl_MaterialParameters material;
	
	vec3 Normal;
	
	if(gl_FrontFacing)
	{
		material = gl_FrontMaterial;
		Normal = normal;
	}
	else
	{
		material = gl_BackMaterial;
		Normal = -normal;
	}
	
	
	for(int i = 0; i < gl_MaxLights; i++)
	{
		vec4 color = vec4(0,0,0,0);
		vec4 dir = pos-gl_LightSource[i].position;
		vec4 ndir = normalize(dir);
		dir.w = 0;
		float dist = length(dir);
		float attenuation = min(1, 1.0/(gl_LightSource[i].constantAttenuation + gl_LightSource[i].linearAttenuation*dist + gl_LightSource[i].quadraticAttenuation*dist*dist));
		//Ambient
		color += max(gl_LightSource[i].ambient*material.ambient, vec4(0,0,0,0));
		// Diffuse
		color += max(attenuation*gl_LightSource[i].diffuse*material.diffuse*dot(ndir,vec4(Normal,0)), vec4(0,0,0,0));
		// Specular
		vec4 R = reflect(ndir, normalize(vec4(Normal, 0)));
		vec4 V = vec4(eyePos,1)-pos;
		V = normalize(V);
		color += max(attenuation*gl_LightSource[i].specular*material.specular*pow(dot(R,V),material.shininess), vec4(0,0,0,0));
		gl_FragColor += color;
	}
}
