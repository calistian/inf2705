#version 130

#define EPSILON 0.01

uniform int indiceFonction;
uniform int localViewer;
uniform int indiceTexture;

uniform float facteurZ;
uniform sampler2D displacementMap;

out vec3 normal;
out vec4 pos;
out vec2 texCoord;

float Fonc( float x, float y )
{
	float z = 0.0;
	if ( indiceFonction == 0 )
		z = ( y*y - x*x );
	else if ( indiceFonction == 1 )
		z = 2.0 * x*y;
	else if ( indiceFonction == 2 )
		z = ( y*sin(2.0*x) * x*cos(2.0*y) );
	else if ( indiceFonction == 3 )
		z = 5.0 * (x*y) / exp(x*x + y*y);
	return facteurZ * z;
}

vec3 Normal(float x, float y)
{
	vec3 res = vec3(0,0,-1);
	res.x = (Fonc(x+EPSILON,y) - Fonc(x-EPSILON,y))/(2*EPSILON);
	res.y = (Fonc(x,y+EPSILON) - Fonc(x,y-EPSILON))/(2*EPSILON);
	return res;
}

void main( void )
{
	vec4 vertex = gl_Vertex;
	vertex.z = Fonc(vertex.x, vertex.y);
	normal = normalize(Normal(vertex.x, vertex.y));
	// transformation standard du sommet (ModelView et Projection)
	gl_Position = gl_ModelViewProjectionMatrix * vertex;
	pos = gl_ModelViewMatrix * vertex;

	// calculer de la position du sommet dans l'espace de l'oeil afin que la carte fasse le découpage demandée par glClipPlane()
	// (On doit initialiser la variable gl_ClipVertex pour que le découpage soit fait par OpenGL.)
	gl_ClipVertex = gl_ModelViewMatrix * vertex;
	gl_ClipVertex /= gl_ClipVertex.w;
	
}
